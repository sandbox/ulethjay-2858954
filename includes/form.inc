<?php

use GatherContent\Content;

/**
 * Implements hook_form_alter() for gathercontent_og_form_alter()
 *
 * Add OG Group selection to gathercontent mapping edit form
 */
function gathercontent_og_gathercontent_mapping_edit_form_alter(&$form, &$form_state, $form_id) {

  // Retrieve current mapping data
  $mapping_entity = entity_load('gathercontent_mapping', array($form['id']['#value']));
  $mapping = reset($mapping_entity);
  $data = unserialize($mapping->data);

  // Retrieve available og group content
  $entities = array();
  $entity_ids = og_get_all_group('node');
  if($entity_ids) {
    $entities = entity_load('node', $entity_ids);
  }

  // Build array of options for the select widget
  $options = array();
  foreach ($entities as $entity) {
    $options[$entity->nid] = $entity->title;
  }

  // Determine default value for select widget if available
  $default_value = NULL;
  if (isset($data['tabog']['elements']['og_group_ref'])) {
    $default_value = $data['tabog']['elements']['og_group_ref'];
  }

  // Build render array for select widget
  $og = array(
    'tabog' => array(
      '#type' => 'fieldset',
      '#title' => 'Group',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      'elements' => array (
        'og_group_ref' => array (
          '#type' => 'select',
          '#options' => $options,
          '#title' => 'Select a Group',
          '#default_value' => $default_value,
          '#empty_option' => '- None -',
        ),
      ),
    ),
  );

  $form['mapping'] = $og + $form['mapping'];

}


/**
 * Implements hook_form_alter() for gathercontent_og_form_alter()
 *
 * Add OG Menu selection to gathercontent import form
 */
function gathercontent_og_gathercontent_import_form_content_select_form_alter(&$form, &$form_state, $form_id) {

  // Determine the gathercontent project id
  $project_id = $form_state['object']->project_id;
  if (isset($form_state['values']['project'])) {
    $project_id = $form_state['values']['project'];
  }

  // Retrieve mapping entities for current project
  $mapping = gathercontent_og_og_group_ref();

  // Retrieve content for current project
  $content_obj = new Content();
  $content = $content_obj->getContents($project_id);

  // Loop through each of the pieces of content about to be imported
  foreach ($content as $item) {

    // Skip through anything that isn't relevant
    if (is_null($item->template_id)) continue;
    if ($item->template_id == 'null') continue;
    if (!isset($mapping[$item->template_id])) continue;
    if (!isset($form['import']['content'][$item->id])) continue;

    // Determine the group id this item will be mapped to
    $gid = (int) $mapping[$item->template_id];

    // Build menu options for this group
    $menus = array();
    $og_menus = og_menu_get_group_menus(array('node' => array($gid)));
    foreach ($og_menus as $menu) {
      $menus[$menu['menu_name']] = $menu['title'];
    }

    $options = array(-1, 'Parent Being Imported') + menu_parent_options($menus, array('mlid' => 0));

    // Replace the previous menu options with our new options
    $form['import']['content'][$item->id]['menu']['#options'] = $options;

  }

}
